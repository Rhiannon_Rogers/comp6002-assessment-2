<?php include_once('functions/functions.php'); ?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>TITLE HERE</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/customstyles.css" type="text/css" >
    </head>
    <body class="backing">
        <!-- Content beings here -->        
        
        <div class="container">
            <div class="row">
                <header class="page-header">
                    <h1>Welcome to Simple Coffee!</h1>
                </header>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-header extraPadding">
                            <h2>Our Coffees</h2>
                        </div>
                        <div class="panel-body customPanel">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>
                                        Coffee
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                </tr>
                                </thead>
                                
                                <?php echo show_all_items(get_all_items("DRINKS"), "index"); ?>

                            </table> 
                        </div>
                        <div class="panel-footer">
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-header extraPadding">
                            <h2>Sides to enjoy</h2>
                        </div>
                        <div class="panel-body customPanel">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>
                                        Sides
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                </tr>
                                </thead>
                                
                                <?php echo show_all_items(get_all_items("SIDES"), "index"); ?>

                            </table>   
                        </div>
                        <div class="panel-footer">
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-header extraPadding">
                            <h2>Call 2 Order</h2>
                        </div>
                        <div class="panel-body customPanel">
                            <h4>NAME: Jeff Kranenburg</h4>
                            <h4>PHONE: 9980349</h4>
                        </div>
                        <div class="panel-footer">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container-fluid">
            <div class="row">
                <div class="bottomFix extraPadding">
                    <h3>346 Somewhere Road, Little Town</h3>
                    <h3><a href="login.php">admin login</a></h3>
                </div>
            </div>
        </div>
        
        <!-- Content ends here -->
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>