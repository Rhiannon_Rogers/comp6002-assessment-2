<?php include_once('functions/functions.php');
session_start();
 loginAdmin(); ?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>TITLE HERE</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/customstyles.css" type="text/css" >
    </head>
    <body class="backing">
        <!-- Content beings here -->        
        <?php 
        if( !isset($_SESSION['login']) )
        {
        ?>
        <div class="container">
            <div class="row">
                <header class="page-header">
                    <h1>Welcome to Simple Coffee!</h1>
                </header>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-header extraPadding">
                            <h2>Please enter in your credentials to login:</h2>
                        </div>
                        <div class="panel-body">
                            <form method="POST">
                                <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Username</span>
                                <input type="text" class="form-control" name="username" placeholder="Username" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Password</span>
                                <input type="password" class="form-control" name="password" placeholder="Password" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                <button type="submit" name="login" class="btn btn-success">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container-fluid">
            <div class="row">
                <div class="bottomFix extraPadding">
                    <h3>346 Somewhere Road, Little Town</h3>
                    <h3><a href="login.php">admin login</a></h3>
                </div>
            </div>
        </div>

        <?php  
        }
        else
        {
            redirect("admin/index.php");
        }
        ?>
        
        <!-- Content ends here -->
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>