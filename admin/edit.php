<?php include_once('../functions/functions.php');
session_start();
 editRecord(); ?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>TITLE HERE</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <!-- Custom CSS -->
        <link rel="stylesheet" href="../css/bootstrap-select.min.css" type="text/css" >
        <link rel="stylesheet" href="../css/customstyles.css" type="text/css" >
        
    </head>
    <body class="backing">
        <!-- Content beings here -->        
        <?php 
        if(isset($_SESSION['login']) == true)
        {
        ?>
        <div class="container">
            <div class="row">
                <header class="page-header">
                    <h1>Welcome to Simple Coffee!</h1>
                </header>
            </div>
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-header extraPadding">
                            <h2>Add a new product to the menu</h2>
                        </div>
                        <div class="panel-body customPanel">
                            
                            <form method="POST">
                                <input type="hidden" name="id" value="<?php echo displayID(); ?>">
                                <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Product Name</span>
                                <input type="text" class="form-control" name="name" placeholder="Name of Drink / Side" aria-describedby="basic-addon1" value="<?php echo displayName(); ?>">
                                </div>
                                <br>
                                <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Product Price</span>
                                <input type="number" class="form-control" name="price" placeholder="0.00" aria-describedby="basic-addon1" value="<?php echo displayPrice(); ?>">
                                </div>
                                <br>
                                <select class="selectpicker" name="cat" title="Choose one of the following...">
                                    <option>Drinks</option>
                                    <option>Sides</option>
                                </select>
                                <br>
                                <br>
                                <button type="submit" name="updateItem" class="btn btn-success">Submit</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container-fluid">
            <div class="row">
                <div class="bottomFix extraPadding">
                    <h3>346 Somewhere Road, Little Town</h3>
                    <h3><a href="../login.php">admin login</a></h3>
                </div>
            </div>
        </div>
        <?php  
        }
        else
        {
        ?>
        <div class="container page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading ">
                            <h1>Simple Coffee</h1>
                        </div>
                        <div class="panel-body customPanel">
                            <h2 class="extraPadding">You do not have access to this page</h2>
                            <h2><a href="../login.php"><button class="btn btn-warning" style="width:200px">Go to the login screen</button></a></h2>
                            <h2><a href="../login.php"><button class="btn btn-info" style="width:200px">Go back to the home screen</button></a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>
        <!-- Content ends here -->
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../js/bootstrap-select.min.js"></script>
    </body>
</html>